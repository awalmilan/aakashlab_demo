<?php

use Illuminate\Http\Request;
// use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::group(['middleware' => ['auth:api','admin']], function () {
 Route::post('saveCompany','Company\CompanyController@createCompany'); 

	Route::post('updateCompany/{id}','Company\CompanyController@updateCompany'); 
	Route::get('getAllCompany','Company\CompanyController@getAllCompanies'); 
	Route::get('getCompanyByID/{id}','Company\CompanyController@getCompanyByID'); 
	Route::post('deleteCompany/','Company\CompanyController@deleteCompany');
	Route::get('getCustomerByID/{id}','Customer\CustomerController@getCustomerByID');
	Route::post('updateCustomer/{id}','Customer\CustomerController@updateCustomer');
	Route::post('deleteCustomer','Customer\CustomerController@deleteCustomer');

});
Route::group(['middleware' => ['api']], function () {  
    Route::post('login','Customer\CustomerController@login'); 
    Route::post('register','Customer\CustomerController@register'); 
    


	
});

Route::any('{path}', function() {
    return response()->json([
    	'error' => true,
        'message' => 'Url not found'
    ], 404);
})->where('path', '.*');