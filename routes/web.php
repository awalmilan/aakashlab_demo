<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Auth::routes(['register' => false]);
Route::group(['middleware' => 'auth'], function () {
    Route::group(['middleware' => 'adminlogin'], function () {
        Route::get('/home', 'HomeController@index')->name('home');
        Route::get('/admins', 'Admin\AdminController@index')->name('admins');
        Route::get('/dashboard', 'Admin\AdminController@index')->name('dashboard');
        Route::get('/users', 'Admin\AdminController@index')->name('users');
        Route::get('/users/create', 'Admin\AdminController@create')->name('user.create');
        Route::post('/users/create', 'Admin\AdminController@store')->name('user.store');
       
        Route::delete('/users/{id}', 'Admin\AdminController@destroy')->name('user.delete');

        Route::get('/users/profile', 'Admin\AdminController@profile')->name('user.profile');
        Route::post('/users/profile', 'Admin\AdminController@profileUpdate')->name('profile.update');
        Route::get('/users/password', 'Admin\AdminController@password')->name('user.password');
        Route::post('/users/password', 'Admin\AdminController@passwordUpdate')->name('password.update');

        Route::get('/customers', 'Admin\AdminController@customers')->name('customers');
        Route::delete('/customers/{id}', 'Admin\AdminController@destroyCustomers')->name('customer.delete');
});
});

Route::get('logout', 'Admin\AdminController@logOut')->name('adminlogout');
 

