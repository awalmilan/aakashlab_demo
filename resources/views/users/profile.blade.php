@extends('layouts.master')
@section('page_title','')

@section('content')
@if (Session::has('flash_mesage'))
		<div class="alert alert-success alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>	
				<strong>{!! Session::get('flash_mesage') !!}</strong>
		</div>
        @endif 
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<!-- DataTales Example -->
<a href="{{ url()->previous() }}" class="btn btn-default padding-remove"><i class="mdi mdi-arrow-left menu-icon sub-page-icon"></i></a><br><br>

                  <h4 class="card-title">My Profile</h4>
                  <form class="forms-sample" method="post" action="{{ route('profile.update') }}" enctype="">
                  @csrf
                
                 
                    <div class="form-group">
                      <label for="exampleInputName1">Name</label>
                      <input type="text" name="name" class="form-control" id="title" value="{{ auth()->user()->name }}">
                    </div>
                 
                    <div class="form-group">
                      <label for="exampleInputEmail3">Email</label>
                      <input type="email" name="email" class="form-control" id="slug" value="{{ auth()->user()->email }}">
                    </div>

                    <button type="submit" class="btn btn-gradient-primary mr-2">Update</button>
                    <a href="{{ route('users') }}" class="btn btn-light">Cancel</a>
                  </form>
  
                  

@endsection

@section('script')

@endsection