@extends('layouts.master')
@section('page_title','')

@section('content')
@if (Session::has('flash_mesage'))
		<div class="alert alert-success alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>{!! Session::get('flash_mesage') !!}</strong>
		</div>
        @endif
                <div class="button-container">


                    <a href="{{route('user.create')}}"><button type="button" class="btn btn-success btn-fw">
                            Create User
                        </button></a><br><br>
                
                </div>

<h4 class="card-title">Users</h4>

<!-- DataTales Example -->
<div class="table-responsive">
<br>
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th> Email </th>

                  

                            
                          <th>Action</th>
                          
                        </tr>
                      </thead>
                      <tbody>

                      @foreach($users as $user)
                        <tr>

                          <td>{{ $user->name }}</td>
                          <td> {{ $user->email }}</td>  
                        
                          <td>
                            
                               
                                <form action="{{ route('user.delete',$user->id) }}" class="delform"
                                      style="display: inline;" method="post"
                                      onsubmit="return confirm('Are you sure')">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-danger" >
                                        Delete
                                    </button>
                                </form>
                          

                              </td>
                             

                        </tr>
                        @endforeach
                          </tbody>

                    </table>


                  </div>

@endsection
