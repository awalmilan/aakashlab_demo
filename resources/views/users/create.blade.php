
@extends('layouts.master')
@section('page_title','')

@section('content')
    @if (Session::has('flash_mesage'))
        <div class="alert alert-success alert-block">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{!! Session::get('flash_mesage') !!}</strong>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <!-- DataTales Example -->
    <a href="{{ url()->previous() }}" class="btn btn-default padding-remove"><i
            class="mdi mdi-arrow-left menu-icon sub-page-icon"></i></a><br><br>

    <h4 class="card-title">Add User</h4>

    <form class="forms-sample" method="post" action="{{ route('user.store') }}" enctype="">
        @csrf


        <div class="form-group">
            <label for="exampleInputEmail3">Name</label>
            <input type="text" name="name" class="form-control" id="slug" placeholder="Name">
        </div>
     
        <div class="form-group">
            <label for="exampleInputEmail3">Email</label>
            <input type="email" name="email" class="form-control" id="slug" placeholder="Email">
        </div>
        <div class="form-group">
            <label for="exampleInputEmail3">Password</label>
            <input type="password" name="password" class="form-control" id="slug" placeholder="Password">
        </div>


        <button type="submit" class="btn btn-gradient-primary mr-2">Submit</button>
        <a href="{{ route('users') }}" class="btn btn-light">Cancel</a>
    </form>
@endsection

@section('script')

@endsection
