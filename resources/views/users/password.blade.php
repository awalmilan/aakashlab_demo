@extends('layouts.master')
@section('page_title','')

@section('content')
@if (Session::has('flash_mesage'))
		<div class="alert alert-success alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>	
				<strong>{!! Session::get('flash_mesage') !!}</strong>
		</div>
        @endif 
        @if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if (Session::has('error'))
		<div class="alert alert-danger alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>	
				<strong>{!! Session::get('error') !!}</strong>
    </div>
    @endif
<!-- DataTales Example -->
<a href="{{ url()->previous() }}" class="btn btn-default padding-remove"><i class="mdi mdi-arrow-left menu-icon sub-page-icon"></i></a><br><br>

                  <h4 class="card-title">Update Password</h4>
                  <form class="forms-sample" method="post" action="{{ route('password.update') }}" enctype="">
                  @csrf
                
                 
                  <div class="form-group">
                    <label class="" for="amount">Current Password</span>
                        </label>
                        <input type="password" id="amount" name="current-password"  required="required" class="form-control">
                        
                    </div>
                    <div class="form-group">
                    <label class="" for="amount">New Password</span>
                        </label>
                        <input type="password" id="amount" name="new-password" required="required" class="form-control">
                        
                    </div>   
                  
                    <div class="form-group">                     
                    <label class="" for="amount">Confirm Password</span>
                        </label>
                        <input type="password" id="amount" name="new-password_confirmation" class="form-control">
                        
                    </div>

                    <button type="submit" class="btn btn-gradient-primary mr-2">Update</button>
                    <a href="{{ route('users') }}" class="btn btn-light">Cancel</a>
                  </form>
  
                  

@endsection

@section('script')

@endsection