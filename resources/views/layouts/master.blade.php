<!DOCTYPE html>
<html lang="en">
<head>
@include('layouts.header')
@yield('styles')
</head>
<body>
<div class="container-scroller">
    <!-- partial:partials/_sidebar.html -->
    
    <!-- partial -->
    <!-- partial:partials/_navbar.html -->
    
    @include('layouts.menu')
    <div class="container-fluid page-body-wrapper">
    <nav class="sidebar sidebar-offcanvas" id="sidebar">
    @include('layouts.sidenav')
    </nav>
    <!-- partial -->
    <div class="main-panel">
        <div class="content-wrapper">
         @yield('page_header')  
          <div class="row">
            <div class="col-12 grid-margin">
              <div class="card">
                <div class="card-body">
                  @yield('content')
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
        <!-- partial:partials/_footer.html -->
        <!-- <footer class="footer">
          <div class="d-sm-flex justify-content-center justify-content-sm-between">
            <span class="text-muted text-center text-sm-left d-block d-sm-inline-block"> </span>
          </div>
        </footer> -->
        <!-- partial -->
      </div>`
    </div>
</div>
  @include('layouts.footer')
  @yield('script')
</body>

</html>