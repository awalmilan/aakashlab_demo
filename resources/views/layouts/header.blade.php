 <!-- Required meta tags -->
 <meta charset="utf-8">
 <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
 <link rel="shortcut icon" type='image/png' href="{{ asset('/images/fav.png') }}">

 <title>Demo Admin Panel</title>

 <!-- plugins:css -->
 <link rel="stylesheet" href="{{ asset('manage/vendors/iconfonts/mdi/css/materialdesignicons.min.css') }}">
 <link rel="stylesheet" href="{{ asset('manage/vendors/css/vendor.bundle.base.css') }}">
 <!-- endinject -->
 <!-- inject:css -->
 <link rel="stylesheet" href="{{ asset('manage/css/style.css') }}">
 <!-- endinject -->
 <link rel="shortcut icon" href="{{ asset('/images/fav.png') }}" />


