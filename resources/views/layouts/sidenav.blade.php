<ul class="nav">
 <!--  <li class="nav-item nav-profile">
    <a href="#" class="nav-link">
      <div class="nav-profile-image">
        <img src="{{ asset('manage/images/faces/user.png') }}" alt="profile">
        <span class="login-status online"></span>
        change to offline or busy as needed
      </div>
      <div class="nav-profile-text d-flex flex-column">
        <span class="font-weight-bold mb-2">{{ Auth::user()->name }}</span>
        <span class="text-secondary text-small">{{ Auth::user()->role }}</span>
      </div>
      <i class="mdi mdi-bookmark-check text-success nav-profile-badge"></i>
    </a>
  </li> -->
  <li class="nav-item">
    <a class="nav-link" href="{{ asset('/admin/dashboard') }}">
      <i class="mdi mdi-home menu-icon menu-icon-main"></i>
      <span class="menu-title">Dashboard</span>
    </a>
  </li>








  <li class="nav-item">
    <a class="nav-link" data-toggle="collapse" href="#ui-basics" aria-expanded="false" aria-controls="ui-basic">
      <i class="mdi mdi-account-box menu-icon menu-icon-main"></i>
      <span class="menu-title">User Management</span>
      <i class="menu-arrow"></i>
    </a>
    <div class="collapse" id="ui-basics">
      <ul class="nav flex-column sub-menu">
        <li class="nav-item"> <a class="nav-link" href="{{ route('users') }}">Users</a></li>

      </ul>
    </div>
  </li>

  <li class="nav-item">
    <a class="nav-link" href="{{ route('customers') }}">
      <i class="mdi mdi-shopping menu-icon menu-icon-main"></i>
      <span class="menu-title">Customer</span>
    </a>
  </li>


    <li class="nav-item">
    <a class="nav-link" href="{{route('adminlogout')}}">
      <i class="mdi mdi-logout menu-icon menu-icon-main"></i>
      <span class="menu-title">Logout</span>
    </a>
  </li>
</ul>
