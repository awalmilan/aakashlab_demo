@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="card-header"><a href="{{ route('admins') }}">{{ __('Admins') }}</a></div>
                    <table>
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                        </thead>
                       <tbody>
                        @foreach($users as $user)
                           <tr>
                               <td>{{ $user->name }} </td>
                               <td>{{ $user->email }} </td>
                           </tr>
                           @endforeach
                       </tbody>
                    </table>
                    <div class="card-header"><a href="{{ route('admins') }}">{{ __('Customers') }}</a></div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
