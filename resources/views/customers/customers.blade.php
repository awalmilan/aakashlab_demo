@extends('layouts.master')
@section('page_title','')

@section('content')
@if (Session::has('flash_mesage'))
		<div class="alert alert-success alert-block">
			<button type="button" class="close" data-dismiss="alert">×</button>
				<strong>{!! Session::get('flash_mesage') !!}</strong>
		</div>
        @endif
                <div class="button-container">

               
                </div>

<h4 class="card-title">Users</h4>

<!-- DataTales Example -->
<div class="table-responsive">
<br>
                    <table class="table">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th> Email </th>
                          <th> Date of Birth </th>
                          <th> Country</th>
                          <th> Profession </th>
                  

                            
                          <th>Action</th>
                          
                        </tr>
                      </thead>
                      <tbody>

                      @foreach($customers as $customer)
                        <tr>

                          <td>{{ $customer->name }}</td>
                          <td> {{ $customer->email }}</td>
                           <td>{{ $customer->dob }}</td>
                          <td> {{ $customer->country }}</td>  
                           <td>{{ $customer->profession }}</td>
                        
                        
                          <td>
                            
                               
                                <form action="{{ route('customer.delete',$customer->id) }}" class="delform"
                                      style="display: inline;" method="post"
                                      onsubmit="return confirm('Are you sure')">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-sm btn-danger" >
                                        Delete
                                    </button>
                                </form>
                          

                              </td>
                             

                        </tr>
                        @endforeach
                          </tbody>

                    </table>


                  </div>

@endsection
