<?php

namespace App\Repositories;

use App\User;
use Carbon\Carbon;
use Auth;
use App\Helpers\Helper;
/**
 * 
 */
class CustomerRepository 
{

	protected $customer = null;

    public function __construct(User $customer){
        $this->customer = $customer;
        
    }

	public function checkCustomer($email)
		{
		
			$customer =  User::where('email',$email)->where('type','customer')->first();
			if (!$customer) {

				return false;
			}else{
				return true;
			}

			
		}

	public function registerCustomer($data)
	{
			
		   
		    $this->customer->type =  'customer';
		    $this->customer->registration_date =  Helper::getCurrentTime('current');
		    $this->customer->fill($data);
		    $this->customer->save();

    		return $this->customer;
	}

	public function updateCustomer($data, $id)
	{		$this->customer = $this->customer->find($id);
		    $this->customer->fill($data);
		    $this->customer->save();

    		return $this->customer;
	}

	public function getCustomerByID($id)
		{

				$customer =  User::where('id',$id)->where('type','customer')
				->first();
				if ($customer) {
					return $this->format($customer);
				}else{
					return false;
				}
		}

	protected function format($customer)
    {
       
            return [
            'name' => $customer->name,
            'email' => $customer->email,
            'dob' => $customer->dob,
            'country' =>$customer->country,
            'profession' =>$customer->profession,
            
        ];    
        
    }


    public function deleteCustomerDetail($id)
    {
    	$this->customer = $this->customer->find($id);
    	$this->customer->delete();
    
    	return true;
    }



}