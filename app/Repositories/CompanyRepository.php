<?php

namespace App\Repositories;

use App\Model\Company;
use Carbon\Carbon;
use Auth;
use App\Helpers\Helper;
/**
 * 
 */
class CompanyRepository 
{

	protected $company = null;

    public function __construct(Company $company){
        $this->company = $company;
        
    }

	public function getUserByEmail($email)
		{
			return User::where('email',$email)
			->firstOrFail();
		}


	public function checkCustomer($email)
		{
		
			$customer =  User::where('email',$email)->where('type','customer')->first();
			if (!$customer) {

				return false;
			}else{
				return true;
			}
			
		}

	public function saveCompany($data)
	{
			
		  
		    $this->company->fill($data);
		    $this->company->save();

    		return $this->company;
	}

	public function updateCompany($data, $id)
	{
		$this->company = $this->company->find($id);
		// dd($this->company);
		$this->company->fill($data);
		$this->company->save();

    	return $this->company;

	}

	public function getAllCompanyList()
	{
		$companies = Company::
                get()->map(function ($user)
				{
					return $this->format($user);
				});

        return $companies;
	}

	  protected function format($company)
    {
       
            return [
            'name' => $company->name,
            'email' => $company->email,
            'address' => $company->address,
            'phone' =>$company->phone,
            
        ];    
        
    }

	public function getCompantByID($id)
		{

				$company = Company::where('id',$id)->first();
				if ($company) {
					return $this->format($company);
				}else{
					return false;
				}
				
		}

    public function deleteCompanyDetail($id)
    {
    	$this->company = $this->company->find($id);
    	$this->company->delete();
    	return true;
    }



}