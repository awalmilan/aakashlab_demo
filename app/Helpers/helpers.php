<?php
namespace App\Helpers;

use Image;
use Carbon\Carbon;

/**
 * 
 */
class Helper
{


   public static function getCurrentTime($state)
    {
        $date = Carbon::now()->format('Y-m-d');
        $now = Carbon::now();
        $year =  date('Y', strtotime($date));
        $month =  date('m', strtotime($date));

        $time['current'] = $now->toDateTimeString();
        $time['timestamp'] = $now->getTimestamp();

        $time['year'] = $year;
         $time['month'] = $month;
         $time['date'] = $date;
        return $time[$state];
    }


}