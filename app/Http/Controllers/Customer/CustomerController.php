<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use App\Repositories\CustomerRepository;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
	private $customerRepository;
  
    public function __construct(CustomerRepository $customerRepository)
    {
        $this->customerRepository = $customerRepository;
    }

    public function login(Request $request)
    {
        

         $loginData = \Validator::make($request->all(), [
                'email' => 'required',
                'password' => 'required'
       
        ]);

         if ($loginData->fails()) {
            $responseArr['error'] = true; 
            $responseArr['message'] = $loginData->errors();
       
            return response()->json($responseArr);
        }

         $credentials = [
            'email' => $request['email'],
            'password' => $request['password'],
        ];

            // $customerexist = $this->customerRepository->checkCustomer($request->email);
            // // dd($customerexist);
            // if ($customerexist == false) {
            //     $responseArr['error'] = true; 
            //     $responseArr['message'] = 'User is not customer. Please login with customer';
           
            //     return response()->json($responseArr);
            // }



        if (!auth()->attempt($credentials)) {
            return response()->json([
            	'error' => true,
            	'message' => 'Invalid Login Credentials']);
        }
        $accessToken = auth()->user()->createToken('authToken')->accessToken;
        return response()->json([
        	'success'=> true,
            'error' => false,
        	'user' => auth()->user(),
        	'token_type' => 'Bearer', 
        	'access_token' => $accessToken]);
    }

    public function register(Request $request)
    {
    	// dd($request->all());
    	$validator = \Validator::make($request->all(), [
       		'name' => 'required|string',
       		'email' => 'required|email|max:255|unique:users',
       		'dob' => 'required|date',
       		'country' => 'required|string',
       		'profession' => 'required|string',
       		'password' => 'required',
	       
    	]);

    	if ($validator->fails()) {
    		 $responseArr['error'] = true; 
        	 $responseArr['message'] = $validator->errors();
       
        	 return response()->json($responseArr);
    	}

        $data['name'] = $request->name;
        $data['dob'] = $request->dob;
        $data['profession'] = $request->profession;
        $data['email'] = $request->email;
        $data['country'] = $request->country;
        $data['password'] = Hash::make($request->password);
        $result = $this->customerRepository->registerCustomer($data);

           

            if ($result['error']==false) {
		    	$responseArr['error'] = false; 
		        $responseArr['message'] = 'Customer registered successfully';
		        $responseArr['data'] = $result;
		       
		        return response()->json($responseArr);
		    }else{

		        return response()->json($result);
		    }

    }

    public function getCustomerByID($id)
    {
    	$customer =  $this->customerRepository->getCustomerByID($id);
        if ($customer) {
             return response()->json(array(
                        'error' => false,
                        'message' => 'Customer Detail',
                 
                        'data' => $customer,
                    ));
            }else{
                return response()->json(array(
                        'error' => true,
                        'message' => 'Empty Data',
                 
                        'data' => $customer,
                    ));
            }
    }

    public function updateCustomer(Request $request, $id)
    {
    	$validator = \Validator::make($request->all(), [
       		'name' => 'required|string',
       		'email' => 'required|email|unique:users,email,'.$id,
       		'dob' => 'sometimes|date',
       		'country' => 'sometimes|string',
       		'profession' => 'sometimes|string',
	       
    	]);

    	if ($validator->fails()) {
    		 $responseArr['error'] = true; 
        	 $responseArr['message'] = $validator->errors();
       
        	 return response()->json($responseArr);
    	}

        $data['name'] = $request->name;
        $data['dob'] = $request->dob;
        $data['profession'] = $request->profession;
        $data['email'] = $request->email;
        $data['country'] = $request->country;
        $data['password'] = Hash::make($request->password);
        $result = $this->customerRepository->updateCustomer($data,$id);

           

            if ($result['error']==false) {
		    	$responseArr['error'] = false; 
		        $responseArr['message'] = 'Customer registered successfully';
		        $responseArr['data'] = $result;
		       
		        return response()->json($responseArr);
		    }else{

		        return response()->json($result);
		    }
    }

    public function deleteCustomer(Request $request)
    {
    	$customer = $this->customerRepository->getCustomerByID($request->id);
        if (!$customer){
            $responseArr['error'] = true; 
	        $responseArr['message'] = 'Data does not exist';
	       
	        return response()->json($responseArr);

        }else{
           
            $success = $this->customerRepository->deleteCustomerDetail($request->id);
            // dd($success);
            if ($success){
                
                $responseArr['error'] = false; 
		        $responseArr['message'] = 'Customer detail successfully deleted';
		        return response()->json($responseArr);
		       
            }else{
                $responseArr['error'] = true; 
		        $responseArr['message'] = 'Something went wrong';
		        return response()->json($responseArr);
		       
            }	
            
        }
    }

}
