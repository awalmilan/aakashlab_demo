<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Hash;
use Auth;

class AdminController extends Controller
{
    public function index()
    {	
    	$users = User::where('type','admin')->get();
    	// dd($users);
    	return view('users.users',compact('users'));
    }

     public function create()
    {	
    	
    	return view('users.create');
    }

    public function store(Request $request)
    {
    	  // dd($request->all());
       $this->validate($request, [
            'name'=>'required',
            'email'=> 'required|unique:users',
            'password'=> 'required|min:8|max:10',
           
        ]);
        $user = new User();
        $user->name = $request->input('name');

        $user->email = $request->input('email');

        $user->password = Hash::make($request->input('password'));	
        $res = $user->save();
            if($res){
                return redirect()->route('users')->with('flash_mesage','User successfully created');
            }else{
                return redirect()->route('users')->with('flash_mesage','User can not be created');

            }
    }

    public function profile()
    {
    	return view('users.profile');
    }

    public function profileUpdate(Request $request)
    {
    	       $id = Auth::user()->id;
        $this->validate($request, [

            'name'=>'required',
            'email' => 'required|email|unique:users,email,'.$id,
            ]
        );

        $user = Auth::user();
        $user->name = $request->input('name');
        $user->email  = $request->input('email');
        $user->save();
        return redirect()->back()->with('flash_mesage','Your profile has been updated');
    }

    public function password()
    {
    	return view('users.password');
    }

    public function passwordUpdate(Request $request){
        //dd($request->all());
        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            return redirect()->back()->with("error","Your current password does not matches with the password you provided. Please try again.");
        }

        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            return redirect()->back()->with("error","New Password cannot be same as your current password. Please choose a different password.");
        }
        $this->validate($request, [

            'current-password' => 'required',
            'new-password' => 'required|string|min:4|confirmed',
            ]
        );
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();

        return redirect()->back()->with("flash_mesage","Password changed successfully !");
    }

    public function logOut(){
        Auth::logout();
        return redirect()->route('login');

	}

	public function destroy($id)
    {
        $users = User::findOrFail($id);
        $users->delete();
        return redirect()->route('users')->with('flash_mesage','User deleted successfully');
    }

    public function customers()
    {
        $customers = User::where('type','customer')->get();
        // dd($users);
        return view('customers.customers',compact('customers'));
    }

    public function destroyCustomers($id)
    {
          $customer = User::findOrFail($id);
        $customer->delete();
        return redirect()->route('customers')->with('flash_mesage','Customer deleted successfully');
    }
}
