<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Repositories\CompanyRepository;

class CompanyController extends Controller
{
	private $companyRepository;
  
    public function __construct(CompanyRepository $companyRepository)
    {
        $this->companyRepository = $companyRepository;
    }
    public function createCompany(Request $request)
    {
    	$validator = \Validator::make($request->all(), [
       		'name' => 'required|string',
       		'email' => 'required|email|max:255|unique:company',
       		'address' => 'required|string',
       		'phone' => 'required|string',
	       
    	]);

    	if ($validator->fails()) {
    		 $responseArr['error'] = true; 
        	 $responseArr['message'] = $validator->errors();
       
        	 return response()->json($responseArr);
    	}

        $data = $request->all();
     
        $result = $this->companyRepository->saveCompany($data);

           

            if ($result['error']==false) {
		    	$responseArr['error'] = false; 
		        $responseArr['message'] = 'Company saved successfully';
		        $responseArr['data'] = $result;
		       
		        return response()->json($responseArr);
		    }else{

		        return response()->json($result);
		    }
    }


     public function updateCompany(Request $request, $id)
    {
    	$validator = \Validator::make($request->all(), [
       		'name' => 'required|string',
       		'email' => 'required|email|unique:company,email,'.$id,
       		'address' => 'required|string',
       		'phone' => 'required|string',
	       
    	]);

    	if ($validator->fails()) {
    		 $responseArr['error'] = true; 
        	 $responseArr['message'] = $validator->errors();
       
        	 return response()->json($responseArr);
    	}

        $data = $request->all();
     
        $result = $this->companyRepository->updateCompany($data, $id);

           

            if ($result['error']==false) {
		    	$responseArr['error'] = false; 
		        $responseArr['message'] = 'Company updated successfully';
		        $responseArr['data'] = $result;
		       
		        return response()->json($responseArr);
		    }else{

		        return response()->json($result);
		    }
    }

    public function getAllCompanies()
    {
    	  $company =  $this->companyRepository->getAllCompanyList();
        // dd($user);
        if ($company) {
             return response()->json(array(
                        'error' => false,
                        'message' => 'Get all company list',
                 
                        'data' => $company,
                    ));
            }else{
                return response()->json(array(
                        'error' => true,
                        'message' => 'Empty Data',
                 
                        'data' => $company,
                    ));
            }
    }

    public function getCompanyByID($id)
    {
    	$company =  $this->companyRepository->getCompantByID($id);
        if ($company) {
             return response()->json(array(
                        'error' => false,
                        'message' => 'Company Detail',
                 
                        'data' => $company,
                    ));
            }else{
                return response()->json(array(
                        'error' => true,
                        'message' => 'Empty Data',
                 
                        'data' => $company,
                    ));
            }
    }

    public function deleteCompany(Request $request)
    {
    	$company = $this->companyRepository->getCompantByID($request->id);
        if (!$company){
            $responseArr['error'] = true; 
	        $responseArr['message'] = 'Data does not exist';
	       
	        return response()->json($responseArr);

        }else{
           
            $success = $this->companyRepository->deleteCompanyDetail($request->id);
            // dd($success);
            if ($success){
                
                $responseArr['error'] = false; 
		        $responseArr['message'] = 'Company detail successfully deleted';
		        return response()->json($responseArr);
		       
            }else{
                $responseArr['error'] = true; 
		        $responseArr['message'] = 'Something went wrong';
		        return response()->json($responseArr);
		       
            }	
            
        }
    }
}
